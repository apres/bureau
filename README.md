## Droits et devoirs du bureau

# Droits

- Deux membres du bureau ou plus peuvent co-opter l'adhésion d'un.e membre.
- Le bureau peut décider de l'exclusion d'un.e membre.  Vote à l'unanimité necessaire.
- Le bureau à des droits de modération sur le serveur discord de l'APRES.
 

## Droits de vote

- Ratification des changements das status et/ou coutumes, aka réglement intérieur, lors de la réunion mensuelle. Unanimité necessaire
- Pour l'élection d'un.e propriétaire du serveur Discord / de l'administrat.rice.eur du site web. Majorité des 2/3 necessaire
- Peut décider de l'exclusion d'un.e membre du bureau. Majorité des 2/3 necessaire et division de la durée de mandat des votant par deux pour ceux ayant voté "pour".

# Devoirs
- Participer à une réunion tous les derniers dimanche du mois.
- Héritent des devoirs des simples membres.
